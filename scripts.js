let product = {
  name: "name",
  price: 1150,
  discount: 15,
  countDiscount: function () {
    return (totalPrice = this.price - (this.price * this.discount) / 100);
  },
};
console.log(product.countDiscount());

let userName = prompt("Введіть своє ім'я");
let userAge = prompt("Введіть свій вік");
function greetings(name, age) {
  alert(`Привіт, мене звати ${name} мені ${age} років`);
}
console.log(greetings(userName, userAge));

// Нашел грубо говоря и пришел только к одному пока что решению,
// но до конца еще его не совсем разобрал (не на все 100%).
// кроме єтого способа еще с помощью lodash но єто отдельное уже
function deepClone(obj, clonedObjects = {}) {
  if (obj === null || typeof obj !== "object") {
    console.log(obj);
    return obj;
  }
  if (clonedObjects.hasOwnProperty(obj)) {
    return clonedObjects[obj];
  }
  let cloned = Array.isArray(obj) ? [] : {};
  clonedObjects[obj] = cloned;
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      cloned[key] = deepClone(obj[key], clonedObjects);
    }
  }

  return cloned;
}
